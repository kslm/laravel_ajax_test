@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-4">
            <div class="Info"> </div>
            <div class="panel panel-primary">
                <div class="panel-heading">Insert New Data</div>
                <div class="panel-body">
                    <form method="post">
                        <div class="form-group">
                            <input type="text" name="title" id="title" class="form-control">
                        </div>
                        <div class="form-group">
                            <input type="number" name="price" id="price" class="form-control">
                        </div>
                        <input type="hidden" name="_token" id="_token" value="{{csrf_token()}}">
                        <div class="form-group">
                            <button type="button" name="btnSave" id="btnSave" class="btn btn-primary btn-lg">Save</button>
                        </div>
                     </form>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <table class="table">
                <tr>
                    <td class="text-primary">Title</td>
                    <td class="text-primary">Price</td>
                    <td class="text-danger"> Delete</td>
                </tr>
                <tbody id="show">

                </tbody>
            </table>
        </div>
    </div>
</div>
@endsection
