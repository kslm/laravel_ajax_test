<?php

namespace App\Http\Controllers;

use App\Test;
use Illuminate\Http\Request;

class testController extends Controller
{
    public function postNewData(Request $request){

        $title = $request['title'];
        $price = $request['price'];
        $t = new Test();
        $t->title=$title;
        $t->price=$price;
        $t->save();

        echo "<div class='alert alert-success'>Insert data Success</div>";

    }

    public function preShowData(){
        $dd = Test::all();
        return view('pre-show')->with(['dd'=>$dd]);
    }
    public function deleteData($id){
        $d = Test::Where('id');
        $d->delete($id);
        return redirect()->back()->with(['dd'=>$d]);
    }


}
