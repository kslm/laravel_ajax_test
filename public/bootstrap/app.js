$(function () {


    showData();
    function showData() {

        $.ajax({
           type : 'get',
            url : 'pre_show',
            success : function (msg) {

                $("#show").html(msg);
            }
        });
        
    }


    $("body").delegate('#btnDel','click',function () {

       var id = $(this).attr('idd');
       var res = confirm('r u sure delete data?');
       if(res){
           $.ajax({
              type : 'get',
               url : 'delete',
               data : {id : id },
               success : function () {
                  showData();

               }
           });
       }

    });


   $("#btnSave").on('click',function () {

       var title = $('#title').val();
       var price = $('#price').val();
       var token = $('#_token').val();

       $.ajax({
          type : 'post',
           url : 'new_data',
           data : {title:title, price:price, _token:token},
           success : function (msg) {
              $('.Info').html(msg);

              $('#title').val('');
              $('#price').val('');
              showData();

           }
       });

   });

});